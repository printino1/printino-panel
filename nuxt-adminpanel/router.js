import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _56dd0618 = () => interopDefault(import('..\\pages\\auth\\index.vue' /* webpackChunkName: "pages/auth/index" */))
const _5aa96e32 = () => interopDefault(import('..\\pages\\auth\\forgetpassword.vue' /* webpackChunkName: "pages/auth/forgetpassword" */))
const _2084a92f = () => interopDefault(import('..\\pages\\auth\\login.vue' /* webpackChunkName: "pages/auth/login" */))
const _9314de8c = () => interopDefault(import('..\\pages\\auth\\smsverification.vue' /* webpackChunkName: "pages/auth/smsverification" */))
const _ac03927a = () => interopDefault(import('..\\pages\\panel\\amin.vue' /* webpackChunkName: "pages/panel/amin" */))
const _19bd3a3e = () => interopDefault(import('..\\pages\\panel\\brands.vue' /* webpackChunkName: "pages/panel/brands" */))
const _f1f94c20 = () => interopDefault(import('..\\pages\\panel\\category.vue' /* webpackChunkName: "pages/panel/category" */))
const _c3d9e034 = () => interopDefault(import('..\\pages\\panel\\comments.vue' /* webpackChunkName: "pages/panel/comments" */))
const _1d373690 = () => interopDefault(import('..\\pages\\panel\\create-product.vue' /* webpackChunkName: "pages/panel/create-product" */))
const _1e28c512 = () => interopDefault(import('..\\pages\\panel\\dashboard.vue' /* webpackChunkName: "pages/panel/dashboard" */))
const _d83df760 = () => interopDefault(import('..\\pages\\panel\\gallery.vue' /* webpackChunkName: "pages/panel/gallery" */))
const _6fbdb100 = () => interopDefault(import('..\\pages\\panel\\pages\\index.vue' /* webpackChunkName: "pages/panel/pages/index" */))
const _4ae58728 = () => interopDefault(import('..\\pages\\panel\\products\\index.vue' /* webpackChunkName: "pages/panel/products/index" */))
const _1c2f97bd = () => interopDefault(import('..\\pages\\panel\\properties\\index.vue' /* webpackChunkName: "pages/panel/properties/index" */))
const _5f6a9cd2 = () => interopDefault(import('..\\pages\\panel\\sellers.vue' /* webpackChunkName: "pages/panel/sellers" */))
const _33c096dc = () => interopDefault(import('..\\pages\\panel\\transaction.vue' /* webpackChunkName: "pages/panel/transaction" */))
const _7ca5a358 = () => interopDefault(import('..\\pages\\panel\\products\\Inventory\\_id.vue' /* webpackChunkName: "pages/panel/products/Inventory/_id" */))
const _c4fb0896 = () => interopDefault(import('..\\pages\\panel\\products\\seller\\_id.vue' /* webpackChunkName: "pages/panel/products/seller/_id" */))
const _02d769d4 = () => interopDefault(import('..\\pages\\panel\\products\\_id.vue' /* webpackChunkName: "pages/panel/products/_id" */))
const _154a6565 = () => interopDefault(import('..\\pages\\panel\\properties\\_id.vue' /* webpackChunkName: "pages/panel/properties/_id" */))
const _461cbedc = () => interopDefault(import('..\\pages\\panel\\pages\\_pageId\\rows\\index.vue' /* webpackChunkName: "pages/panel/pages/_pageId/rows/index" */))
const _fdb9d70e = () => interopDefault(import('..\\pages\\panel\\pages\\_pageId\\rows\\_rowId\\cols.vue' /* webpackChunkName: "pages/panel/pages/_pageId/rows/_rowId/cols" */))
const _182f9e80 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/auth",
    component: _56dd0618,
    name: "auth"
  }, {
    path: "/auth/forgetpassword",
    component: _5aa96e32,
    name: "auth-forgetpassword"
  }, {
    path: "/auth/login",
    component: _2084a92f,
    name: "auth-login"
  }, {
    path: "/auth/smsverification",
    component: _9314de8c,
    name: "auth-smsverification"
  }, {
    path: "/panel/amin",
    component: _ac03927a,
    name: "panel-amin"
  }, {
    path: "/panel/brands",
    component: _19bd3a3e,
    name: "panel-brands"
  }, {
    path: "/panel/category",
    component: _f1f94c20,
    name: "panel-category"
  }, {
    path: "/panel/comments",
    component: _c3d9e034,
    name: "panel-comments"
  }, {
    path: "/panel/create-product",
    component: _1d373690,
    name: "panel-create-product"
  }, {
    path: "/panel/dashboard",
    component: _1e28c512,
    name: "panel-dashboard"
  }, {
    path: "/panel/gallery",
    component: _d83df760,
    name: "panel-gallery"
  }, {
    path: "/panel/pages",
    component: _6fbdb100,
    name: "panel-pages"
  }, {
    path: "/panel/products",
    component: _4ae58728,
    name: "panel-products"
  }, {
    path: "/panel/properties",
    component: _1c2f97bd,
    name: "panel-properties"
  }, {
    path: "/panel/sellers",
    component: _5f6a9cd2,
    name: "panel-sellers"
  }, {
    path: "/panel/transaction",
    component: _33c096dc,
    name: "panel-transaction"
  }, {
    path: "/panel/products/Inventory/:id?",
    component: _7ca5a358,
    name: "panel-products-Inventory-id"
  }, {
    path: "/panel/products/seller/:id?",
    component: _c4fb0896,
    name: "panel-products-seller-id"
  }, {
    path: "/panel/products/:id",
    component: _02d769d4,
    name: "panel-products-id"
  }, {
    path: "/panel/properties/:id",
    component: _154a6565,
    name: "panel-properties-id"
  }, {
    path: "/panel/pages/:pageId/rows",
    component: _461cbedc,
    name: "panel-pages-pageId-rows"
  }, {
    path: "/panel/pages/:pageId/rows/:rowId/cols",
    component: _fdb9d70e,
    name: "panel-pages-pageId-rows-rowId-cols"
  }, {
    path: "/",
    component: _182f9e80,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
