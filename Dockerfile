FROM node:lts-buster

WORKDIR /opt/web

COPY package.json .

RUN npm i -f

COPY . .

ENV NODE_OPTIONS=--openssl-legacy-provider

RUN npm run build

RUN npm run generate

EXPOSE 3000

CMD npm start
